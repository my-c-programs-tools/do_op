/*
** main.c for do-op in /home/jordan/rendu/Jour08-C/verove_j/do-op
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Oct 25 13:28:05 2016 VEROVE Jordan
** Last update Wed Oct  9 09:46:39 2019 VEROVE Jordan
*/

int	my_getnbr(char *str);
void	do_op(int v1, char op, int v2);
int	check_value(char *str);
int	check_op(char *op, char *v2);

int	main(int ac, char **av)
{
  if (ac != 4)
    return (-1);
  if (check_value(av[1]) == -1 || check_value(av[3]) == -1)
    return (-1);
  if (check_op(av[2], av[3]) == -1)
    return (-1);
  do_op(my_getnbr(av[1]), av[2][0], my_getnbr(av[3]));
  return (0);
}
