/*
** tools.c for do_op in 
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Oct 25 13:44:10 2016 VEROVE Jordan
** Last update Tue Oct 25 19:30:37 2016 VEROVE Jordan
*/

#include <unistd.h>

void		my_putchar(char c)
{
  write(1, &c, 1);
}

void		my_putstr(char *str)
{
  int		i;

  i = 0;
  while (str[i] != 0)
    my_putchar(str[i++]);
}

int		my_strlen(char *str)
{
  int		i;

  i = 0;
  while (str[i] != 0)
    i++;
  return (i);
}

int		my_getnbr(char *str)
{
  int		i;
  int		nb;
  int		neg;

  i = 0;
  nb = 0;
  neg = 0;
  while (str[i] != '\0')
    {
      if ((str[i] < '0' || str[i] > '9') && str[i] != '+' && str[i] != '-')
	return (((neg % 2) == 0) ? (nb) : ((nb) * -1));
      if (str[i] == '-')
	neg += 1;
      else if (str[i] != '+')
	{
	  nb = nb * 10;
	  nb = nb + str[i] - 48;
	}
      i++;
    }
  if (neg % 2 == 1)
    return (-1 * nb);
  return (nb);
}
