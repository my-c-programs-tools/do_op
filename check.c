/*
** check.c for do_op in /home/jordan/rendu/Jour08-C/verove_j/do-op
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Oct 25 14:09:36 2016 VEROVE Jordan
** Last update Wed Oct  9 09:30:33 2019 VEROVE Jordan
*/

int	my_strlen(char *str);
void	my_putstr(char *str);
int	my_getnbr(char *str);

int	check_value(char *v)
{
  int	check_num;
  int	i;

  check_num = 0;
  i = 0;
  if (my_getnbr(v) == 0)
    {
      while (v[i] != 0)
	{
	  if (v[i] != '-' && v[i] != '+' && (v[i] > '9' || v[i] < '0'))
	    check_num = 1;
	  else if (v[i] == '0' && check_num != 1)
	    return (0);
	  i++;
	}
      my_putstr("Syntax Error\n");
      return (-1);
    }
  return (0);
}

int		check_op(char *op, char *v2)
{
  if ((my_strlen(op) != 1)
      || !(op[0] == '+' || op[0] == '-' || op[0] == '%' || op[0] == '/' || op[0] == '*')
      || ((op[0] == '/' || op[0] == '%') && (my_getnbr(v2) == 0)))
    {
      my_putstr("Syntax Error. Please enter a valid operation.\n");
      return (-1);
    }
  return (0);
}
