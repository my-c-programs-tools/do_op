/*
** tools2.c for do_op in /home/jordan/rendu/Jour08-C/verove_j/do-op
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Oct 25 17:35:05 2016 VEROVE Jordan
** Last update Wed Oct  9 09:44:26 2019 VEROVE Jordan
*/

void	my_putchar(char c);

void	my_put_nbr_check_neg(int nb, int check)
{
  if (nb < 0 && check == 0)
    my_putchar('-');
  if (nb >= 10 || nb <= -10)
    {
      my_put_nbr_check_neg(nb / 10, 1);
      my_put_nbr_check_neg(nb % 10, 1);
    }
  else if (nb >= 0)
    my_putchar(nb + 48);
  else if (nb < 0)
    my_putchar((-nb * 2) + nb + 48);
}

void	my_put_nbr(int nb)
{
  my_put_nbr_check_neg(nb, 0);
}
