/*
** operation.c for do_op in /home/jordan/rendu/Jour08-C/verove_j/do-op
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Oct 25 14:37:48 2016 VEROVE Jordan
** Last update Wed Oct  9 09:45:25 2019 VEROVE Jordan
*/

void	my_put_nbr(int nb);

void	add(int v1, int v2)
{
  my_put_nbr(v1 + v2);
}

void	sub(int v1, int v2)
{
  my_put_nbr(v1 - v2);
}

void	mul(int v1, int v2)
{
  my_put_nbr(v1 * v2);
}

void	div(int v1, int v2)
{
  my_put_nbr(v1 / v2);
}

void	mod(int v1, int v2)
{
  my_put_nbr(v1 % v2);
}
