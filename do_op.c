/*
** do_op.c for do_op in /home/jordan/rendu/Jour08-C/verove_j/do-op
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Tue Oct 25 14:19:03 2016 VEROVE Jordan
** Last update Tue Oct 25 19:30:08 2016 VEROVE Jordan
*/

void	add(int v1, int v2);
void	sub(int v1, int v2);
void	mul(int v1, int v2);
void	div(int v1, int v2);
void	mod(int v1, int v2);
void	my_putchar(char c);

int	get_operator(char op, char *operator)
{
  int	i;

  i = 0;
  while (i < 5)
    {
      if (op == operator[i])
	return (i);
      i++;
    }
  return (0);
}

void	do_op(int v1, char op, int v2)
{
  void	(*func_ptr[5])(int, int);
  char	operator[5];

  func_ptr[0] = &add;
  func_ptr[1] = &sub;
  func_ptr[2] = &mul;
  func_ptr[3] = &div;
  func_ptr[4] = &mod;  
  operator[0] = '+';
  operator[1] = '-';
  operator[2] = '*';
  operator[3] = '/';
  operator[4] = '%';
  func_ptr[get_operator(op, operator)](v1, v2);
  my_putchar('\n');
}
