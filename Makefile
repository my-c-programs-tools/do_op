##
## Makefile for do_op in /home/jordan/rendu/Jour08-C/verove_j/do-op
## 
## Made by VEROVE Jordan
## Login   <verove_j@etna-alternance.net>
## 
## Started on  Tue Oct 25 17:43:10 2016 VEROVE Jordan
## Last update Tue Oct 25 17:53:34 2016 VEROVE Jordan
##

CC =		gcc

CFLAGS =	-Wall -Werror -Wextra

NAME =		do-op

SRC =		main.c \
		tools.c \
		tools2.c \
		check.c \
		do_op.c \
		operation.c

OBJ =		$(SRC:.c=.o)

RM =		rm -rf

$(NAME):	$(OBJ)
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
